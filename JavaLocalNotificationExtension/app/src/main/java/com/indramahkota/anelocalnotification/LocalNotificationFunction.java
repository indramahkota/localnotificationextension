package com.indramahkota.anelocalnotification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.distriqt.extension.util.Resources;

public class LocalNotificationFunction implements FREFunction {

    private int currentNotificationID = 1;
    private static final String ERROR_EVENT = "error_event";

    @Override
    public FREObject call(FREContext context, FREObject[] args) {
        try {
            NotificationExtension.log("Trying to send LocalNotification");

            String title = args[0].getAsString();
            String message = args[1].getAsString();
            String tickertext = args[2].getAsString();
            Context appContext = context.getActivity().getApplicationContext();

            NotificationManager notificationManager = (NotificationManager)appContext.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder notificationBuilder;

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                NotificationChannel channel = new NotificationChannel("Channel1", "Penting", NotificationManager.IMPORTANCE_HIGH);
                channel.enableLights(true);
                channel.enableVibration(true);
                channel.setDescription("Notifikasi penting!");
                notificationManager.createNotificationChannel(channel);

                notificationBuilder = new NotificationCompat.Builder(appContext, "Channel1");
            }
            else {
                notificationBuilder = new NotificationCompat.Builder(appContext, "");
            }

            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            int smallIconId = Resources.getResourseIdByName(appContext.getPackageName(), "drawable", "app_icon");
            int largeIconId = Resources.getResourseIdByName(appContext.getPackageName(), "drawable", "app_icon");
            Bitmap largeIcon = BitmapFactory.decodeResource(appContext.getResources(), largeIconId);

            if (largeIcon != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                largeIcon = getCircleBitmap(largeIcon);
            }

            Intent notificationIntent = new Intent(appContext, NotificationActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(appContext, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            notificationBuilder.setContentTitle(title)
                    .setContentText(message)
                    .setTicker(tickertext)
                    .setSmallIcon(smallIconId)
                    .setLargeIcon(largeIcon)
                    .setSound(soundUri)
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setColor(0xFF2DA9F9)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentIntent(contentIntent);

            Notification notification = notificationBuilder.build();
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notification.defaults |= Notification.DEFAULT_SOUND;

            notificationManager.notify(currentNotificationID, notification);
            currentNotificationID++;

        } catch (Exception e) {
            e.printStackTrace();
            context.dispatchStatusEventAsync("LOGGING" + ": " + e, ERROR_EVENT);
        }

        return null;
    }

    private Bitmap getCircleBitmap(Bitmap bitmap)
    {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }
}
