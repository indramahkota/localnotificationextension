package com.indramahkota.anelocalnotification;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;

import java.util.HashMap;
import java.util.Map;

public class NotificationExtensionContext extends FREContext {

    private static final String SEND_NOTIFICATION = "send_notification";

    @Override
    public void dispose() { NotificationExtension.context = null; }

    @Override
    public Map<String, FREFunction> getFunctions() {
        Map<String, FREFunction> map = new HashMap<>();
        map.put(SEND_NOTIFICATION, new LocalNotificationFunction());
        return map;
    }
}