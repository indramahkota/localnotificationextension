package com.indramahkota.anelocalnotification;

import android.util.Log;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREExtension;

public class NotificationExtension implements FREExtension {

    public static FREContext context;

    @Override
    public FREContext createContext(String extId) {
        return context = new NotificationExtensionContext();
    }

    @Override
    public void initialize() {}

    @Override
    public void dispose() {
        if (context != null) {
            context.dispose();
            context = null;
        }
    }

    public static void log(String message)
    {
        String TAG = "LocalNotif";
        Log.d(TAG, message);

        if (context != null)
        {
            context.dispatchStatusEventAsync("LOGGING", message);
        }
    }
}
