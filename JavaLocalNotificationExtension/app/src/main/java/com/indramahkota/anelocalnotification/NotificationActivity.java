package com.indramahkota.anelocalnotification;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class NotificationActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Intent intent;
        try {
            NotificationExtension.log("NotificationActivity startActivity");
            intent = new Intent(this, Class.forName(this.getPackageName() + ".AppEntry"));
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        NotificationExtension.log("NotificationActivity finish()");

        finish();
    }
}