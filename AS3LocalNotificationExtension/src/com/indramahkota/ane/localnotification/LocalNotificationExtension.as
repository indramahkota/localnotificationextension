package com.indramahkota.ane.localnotification
{
    import flash.events.ErrorEvent;
    import flash.events.EventDispatcher;
    import flash.events.StatusEvent;
    import flash.external.ExtensionContext;
    import flash.system.Capabilities;

    public class LocalNotificationExtension extends EventDispatcher
    {
        protected var _context:ExtensionContext;
        public static const EXTENSION_ID:String = "com.indramahkota.ane.LocalNotification";

        public function LocalNotificationExtension()
        {
            if (!isSupported)
                throw new Error("LocalNotificationExtension is not supported on this platform. Use LocalNotificationExtension.isSupported getter!");

            _context = ExtensionContext.createExtensionContext(EXTENSION_ID, null);
            _context.addEventListener(StatusEvent.STATUS, _onStatus);
        }

        public static function get isSupported():Boolean
        {
            return Capabilities.manufacturer.indexOf("Android") > -1;
        }

        public function show(title:String = "Judul", message:String = "Pesan", tickertext:String = "Ticker Text"):void
        {
            _context.call("send_notification", title, message, tickertext);
        }

        public function dispose():void
        {
            if (_context != null)
            {
                 _context.removeEventListener(StatusEvent.STATUS, _onStatus);
                _context.dispose();
                _context = null;
            }
        }        

        private function _onStatus(e:StatusEvent):void
        {
            trace(e.level);

            const ERROR_EVENT:String = "error_event";

            switch (e.level)
            {
                case ERROR_EVENT:
                    dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, e.code));
                    break;
                default:
                    dispatchEvent(e);
            }
        }
    }
}