# Aplikasi Pembelajaran matematika

## SDK
* [AIR SDK & Compiler](https://www.adobe.com/devnet/air/air-sdk-download.html)

## Framework
* [Starling](https://github.com/Gamua/Starling-Framework)

## User Interface
* [Feathers](https://github.com/BowlerHatLLC/feathers)

## Tool
* [MathML AS3](https://github.com/sevdanski/MathML_AS3)
* [Starling Extension Adobe Animate](https://github.com/Gamua/Starling-Extension-Adobe-Animate/tree/master/src)
	
## Forum
* [Starling Forum](https://forum.starling-framework.org/)
* [AS3Lang Community](https://discuss.as3lang.org/)
	
## Reference
* [Adobe AIR](https://help.adobe.com/en_US/air/build/index.html)
* [Adobe Flash Platform](https://help.adobe.com/en_US/as3/dev/index.html)
* [Regular Expression (RegExp)](http://masputih.com/2015/11/belajar-regular-expression)
* [Working with the AIR APIs](https://help.adobe.com/en_US/air/build/WS5b3ccc516d4fbf351e63e3d118666ade46-7ff3.html)
* [Text Justification Algorithm](https://www.rose-hulman.edu/class/csse/csse221/200910/Projects/Markov/justification.html)
* [Convert text to XML](https://help.adobe.com/en_US/ActionScript/3.0_ProgrammingAS3/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e6d.html)
* [Sevenson MathML Website](http://www.sevenson.com.au/actionscript/mathml/)
* [Justify Content](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Aligning_Items_in_a_Flex_Container)
* [Timer Count Down](http://www.riacodes.com/flash/new-year-countdown-with-as3/)
* [Wandah Article](https://unnes.academia.edu/WandahWibawanto)
* [Wandah Website](http://www.wandah.org)
* [Dynamic Video Player As3](https://code.tutsplus.com/tutorials/build-a-dynamic-video-player-with-actionscript-3-part-1-of-3--active-2931)
* [Android SplashScreen Adobe AIR](https://airnativeextensions.github.io/tutorials/android-splash-screen)
* [Get word from string AS3](https://stackoverflow.com/questions/18453440/get-word-from-string-as3)
* [Get BitmapData From a DisplayObject](https://stackoverflow.com/questions/6953326/get-bitmapdata-from-a-displayobject-included-transparent-area-and-effect-area)
* [Regexp Online Tool website](https://regexr.com/)
* [UI Patterns](http://ui-patterns.com/)
* [Extension Id](https://airnativeextensions.github.io/tutorials/getting-started-flashdevelop)
	
# Note

```
C:\Users\Indra Mahkota\AppData\Roaming\MediaPembelajaran\Local Store\
	#SharedObjects\MediaPembelajaran.swf\mediapembelajaran.sol (Read SharedObject)
C:\Program Files\Air SDK\lib\android\lib\resources\app_entry\res\values (Change styles.xml)
C:\Users\Indra Mahkota (Delete mm.cfg)
```

```
so = [object #1, class 'SharedObject'] {
data: [object #0, class 'Object'] {
		nilai: "20",
		avatar: "avatar_6s",
		indexAvatar: "5",
		nama: "jhgfds",
	}
}
```

```xml
<resources>
	<style name="Theme.NoShadow" parent="android:style/Theme.NoTitleBar">
		<item name="android:windowTranslucentStatus">true</item>
	</style>
</resources>
```

# Android Splash Screen

## Modifying the AIR SDK

The simplest method to give your AIR application a splash screen on Android is
to change the theme of the main AIR activity to contain an image and a background colour.
This method generally requires no modification to your application code, however you will
have to modify your version of the AIR SDK. To get started open up the AIR SDK styles.xml
file located at AIRSDK/lib/android/lib/resources/app_entry/res/values/styles.xml

This file should contain the following:

```xml
<resources>
    <style name="Theme.NoShadow" parent="android:style/Theme.NoTitleBar">
        <item name="android:windowContentOverlay">@null</item>
    </style>
</resources>
```

To start, remove android:windowContentOverlay and add a android:windowBackground as below:
	
```xml
<resources>
    <style name="Theme.NoShadow" parent="android:style/Theme.NoTitleBar">
        <item name="android:windowBackground">@drawable/splash_background</item>
    </style>
</resources>
```
This will set a splash_background resource as the background.
Lets create this resource by adding a splash_background.xml file at the following
location: AIRSDK/lib/android/lib/resources/app_entry/res/drawable/splash_background.xml
This is where we will create the background for your application which will appear as the splasg screen.

## Centered Application Icon

Tempatkan splash_background.xml di C:\airsdk29\lib\android\lib\resources\captive_runtime\res\drawable
A simple splash screen is to use your application icon and center it in the view with a background colour.
Add the following content to the splash_background.xml file:
	
```xml
<?xml version="1.0" encoding="utf-8"?>
<layer-list xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:drawable="@color/splash_background_color" />
    <item>
        <bitmap
            android:gravity="center"
            android:src="@mipmap/icon" />
    </item>
</layer-list>
```

We will define the background colour in the colors.xml
located at AIRSDK/lib/android/lib/resources/app_entry/res/values/colors.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <color name="splash_background_color">#ff0000</color>
</resources>
```

Change this background colour to suit your application.
Once you’ve made these changes to the AIR SDK any Android application
that is built with this AIR SDK will display your splash screen.

## Image

Alternatively if you want to use a custom image as the splash screen you can add
an image file to the resources to use as the background. For example lets add a splash_image.png
to the drawable directory (alongside splash_background.xml), you then need to modify
splash_background.xml to use this image as below:
	
```xml
<?xml version="1.0" encoding="utf-8"?>
<layer-list xmlns:android="http://schemas.android.com/apk/res/android">
    <item>
        <bitmap 
            android:gravity="center"
            android:src="@drawable/splash_image" />
    </item>
</layer-list>
```

## Reverting

To revert your changes you simply revert the styles.xml to the original contents above.

# Set -swf-version pada Flash Develop

Workaround: Project --> Properties --> Compiler Options --> Additional Compiler Options --> -swf-version=40