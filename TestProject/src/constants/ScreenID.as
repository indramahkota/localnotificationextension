﻿package constants
{
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class ScreenID
	{
		public static const INTROSCREEN:String = "intro";
		public static const DAFTARSCREEN:String = "login";
		public static const MENUUTAMA:String = "menuutama";
	}
}