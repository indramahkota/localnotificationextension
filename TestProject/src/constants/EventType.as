package constants
{
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class EventType
	{
		public static const SHOW_DAFTAR_SCREEN:String = "showDaftar";
		public static const SHOW_MENU_UTAMA:String = "showMenuUtama";
	}
}