package {
	import feathers.utils.ScreenDensityScaleFactorManager;

	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.system.Capabilities;
	import flash.system.System;
	import flash.utils.ByteArray;
	import flash.utils.setTimeout;

	import starling.core.Starling;
	import starling.events.Event;
	import starling.extensions.animate.AnimAssetManager;
	
	[SWF(width = "320", height = "480", frameRate = "60", backgroundColor = "#ffffff")]
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class NotificationTest extends Sprite	{
		private var background:Loader;
		private var starling:Starling;
		private var scaler:ScreenDensityScaleFactorManager;
		
		public function NotificationTest() {
			if(stage) {
				stage.addEventListener(flash.events.Event.DEACTIVATE, deactivateHandler, false, 0, true);
				stage.scaleMode = StageScaleMode.NO_SCALE;
				stage.align = StageAlign.TOP_LEFT;
			}
			mouseEnabled = mouseChildren = false;

			starling = new Starling(Main, stage);
			starling.skipUnchangedFrames = true;
			starling.antiAliasing = 4;
			starling.start();

			scaler = new ScreenDensityScaleFactorManager(starling);
			starling.addEventListener(starling.events.Event.ROOT_CREATED, function():void {
                loadAssets(2, startApp);
            });
			initElements();
		}

		private function loadAssets(scaleFactor:int, onComplete:Function):void {
            var appDir:File = File.applicationDirectory;
            var assets:AnimAssetManager = new AnimAssetManager();
            assets.verbose = Capabilities.isDebugger;
            assets.enqueue(
                    appDir.resolvePath("assets/animations/intro/"),
					appDir.resolvePath("assets/images"),
                    appDir.resolvePath("assets/sounds/")
            );
            assets.loadQueue(onLoadComplete, onLoadError);

            function onLoadComplete():void {
                System.pauseForGCIfCollectionImminent(0);
                System.gc();
                onComplete(assets);
            }

            function onLoadError(error:String):void {
                trace("Error while loading assets: " + error);
            }
        }

		private function startApp(assets:AnimAssetManager):void {
            var main:Main = starling.root as Main;
            main.start(assets);
            setTimeout(removeElements, 150);
        }

		private function removeElements():void {
            if(background) {
                removeChild(background);
                background = null;
            }
        }

		private function initElements():void {
			var bgWidth:Number = 1080, bgHeight:Number = 1920;
			var thisScale:Number = stage.fullScreenHeight / bgHeight;
            var bgFile:File = File.applicationDirectory.resolvePath("assets/background.png");
            var bytes:ByteArray = new ByteArray();
            var stream:FileStream = new FileStream();
            stream.open(bgFile, FileMode.READ);
            stream.readBytes(bytes, 0, stream.bytesAvailable);
            stream.close();

            background = new Loader();
            background.loadBytes(bytes);
			background.scaleX = background.scaleY = thisScale;
			background.x = (stage.fullScreenWidth - bgWidth * thisScale) / 2;
            background.contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE,
			function(e:Object):void	{
				(background.content as Bitmap).smoothing = true;
			});
			addChild(background);
        }

		private function deactivateHandler(event:flash.events.Event):void {
			starling.stop(true);
			stage.addEventListener(flash.events.Event.ACTIVATE, activateHandler, false, 0, true);
		}
		
		private function activateHandler(event:flash.events.Event):void	{
			stage.removeEventListener(flash.events.Event.ACTIVATE, activateHandler);
			starling.start();
		}
	}
}
