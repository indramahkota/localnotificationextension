package utilities {
	import starling.display.Mesh;
	import starling.rendering.IndexData;
	import starling.rendering.VertexData;
	import starling.rendering.VertexDataFormat;
 
	public final class RoundedQuad extends Mesh {
		public static const VERTEX_FORMAT:VertexDataFormat =
			VertexDataFormat.fromString("position:float2, color:bytes4");
 
		public static const iCornerSides:int = 12;
 
		private var _tint:uint;
		private var _radius:Number;
		private var _width:Number;
		private var _height:Number
		private var fAngle:Number;
		private var fCos:Number;
		private var fSin:Number;
		private var iVerts:int;
		private var iTris:int;
 
		public function RoundedQuad(radius:Number, width:int, height:int, color:uint = 0xffffff) {
			var vData:VertexData, iData:IndexData;
 
			_radius = radius;
			_tint = color;
			_width = width;
			_height = height;
 
			fAngle= Math.PI / (2 * iCornerSides);
			fCos = Math.cos(fAngle);
			fSin = Math.sin(fAngle);
 
			iTris = 4 * (1 + iCornerSides);
			iVerts = 1 + iTris;
 
			vData = new VertexData(VERTEX_FORMAT, iVerts);
			iData = new IndexData(3 * iTris);
 
			super(vData, iData);
 
			setupVertices();
			updateVertices();
			updateColours();
			setRequiresRedraw();
		}
 
		public function set fRadius(val:Number):void {
			if(_radius != val) {
				_radius = val;
				updateVertices();
				setRequiresRedraw();
			}
		}
 
		public function set fWidth(val:Number):void {
			if(_width != val) {
				_width = val;
				updateVertices();
				setRequiresRedraw();
			}
		}
 
		public function set fHeight(val:Number):void {
			if(_height != val) {
				_height = val;
				updateVertices();
				setRequiresRedraw();
			}
		}
 
		public function set iColour(val:uint):void {
			if(_tint != val) {
				_tint = val;
				updateColours();
				setRequiresRedraw();
			}
		}
 
		private function updateColours():void {
			var vData:VertexData = vertexData;
			vData.colorize( "color", _tint, 1.0, 0, iVerts);
		}
 
		private function updateVertices():void {
			var iVert:int, fX:Number, fY:Number;
			var fX0:Number = -1.0;
			var fY0:Number =  0.0;
			var iCornerVerts:int = 1 + iCornerSides;
 
			var vData:VertexData = vertexData;
			vData.setPoint(0, "position", _width / 2, _height / 2);
 
			for(iVert = 0; iVert < iCornerVerts; iVert++) {
				fX =  fX0 * _radius;
				fY =  fY0 * _radius;

				vData.setPoint(iVert + 1, "position", fX + _radius, fY + _radius);
				vData.setPoint(iVert + 1 + iCornerVerts, "position", -fY + _width - _radius, fX + _radius);
				vData.setPoint(iVert + 1 + 2 * iCornerVerts, "position", -fX + _width - _radius, -fY + _height - _radius);
				vData.setPoint(iVert + 1 + 3 * iCornerVerts, "position", fY + _radius, -fX + _height - _radius);
 
				fX = fX0 * fCos - fY0 * fSin;
				fY = fX0 * fSin + fY0 * fCos;
				fX0 = fX;
				fY0 = fY;
			}
		}
 
		private function setupVertices():void {
			var iTri:int;
			var vData:VertexData = vertexData;
			var iData:IndexData = indexData;
			iData.numIndices = 0;
 
			for(iTri = 0; iTri < iTris; iTri++)
				iData.addTriangle(0, iTri + 1, 1 + (iTri  + 1) % iTris)
		}
	}
}