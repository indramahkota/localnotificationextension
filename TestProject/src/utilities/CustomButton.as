package utilities {
	import feathers.controls.Button;

	import flash.geom.Point;

	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class CustomButton extends Button {
		private var tween:Tween;
		private var radialEffect:RoundedQuad;
		
		public function CustomButton() {
			this.addEventListener(TouchEvent.TOUCH, touchHandler);
		}
		
		protected function touchHandler(event:TouchEvent):void {
			mask = new RoundedQuad(5, bounds.width, bounds.height);
			
			if(!_isEnabled)
				return;

			var isInBounds:Boolean;
			var touch:Touch = event.getTouch(this);

			if(touch) {
                if(touch.phase == TouchPhase.BEGAN) {
					if(radialEffect != null) radialEffect.dispose();
					createcircleTween(touch.getLocation(this));
                } else if(touch.phase == TouchPhase.ENDED) {
					if(tween != null && !tween.isComplete) {
						tween.onComplete = function():void {
							radialEffect.dispose();
							Starling.juggler.remove(tween);
						};
					}
				}
            }
		}
		
		private function createcircleTween(position:Point):void {
			radialEffect = new RoundedQuad(50, 100, 100, 0xd2d2d2);
			radialEffect.alpha = 0.5;
			radialEffect.touchable = false;
			radialEffect.x = position.x, radialEffect.y = position.y;
			radialEffect.alignPivot();
			radialEffect.scale = 0;
			
			if(this.defaultSkin != null)
				addChildAt(radialEffect, 1);
			else
				addChildAt(radialEffect, 0);
			
			if(Starling.juggler.contains(tween))
				Starling.juggler.remove(tween);
			
			tween = new Tween(radialEffect, 1, Transitions.EASE_OUT);
			tween.fadeTo(0);
			tween.scaleTo(1);
			Starling.juggler.add(tween);
		}
	}
}