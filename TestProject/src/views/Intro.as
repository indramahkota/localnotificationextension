package views{

    import com.freshplanet.ane.AirFacebook.FBDefaultAudience;
    import com.freshplanet.ane.AirFacebook.FBLoginBehaviorAndroid;
    import com.freshplanet.ane.AirFacebook.FBLoginBehaviorIOS;
    import com.freshplanet.ane.AirFacebook.FBShareDialogModeAndroid;
    import com.freshplanet.ane.AirFacebook.FBShareDialogModeIOS;
    import com.freshplanet.ane.AirFacebook.Facebook;
    import com.freshplanet.ane.KeyboardSize.MeasureKeyboard;

    import constants.EventType;

    import feathers.controls.Screen;

    import flash.utils.setTimeout;

    import starling.core.Starling;
    import starling.events.Event;
    import starling.extensions.animate.Animation;

    public class Intro extends Screen {
        private var intro:Animation;
        protected var _data:NavigatorData;
        
        public function Intro(){
            super();
        }

        public function get data():NavigatorData  { return _data; }
		
		public function set data(value:NavigatorData):void  { _data = value; }

        override protected function initialize():void {
            intro = Main.assets.createAnimation("intro");
			intro.scaleX = intro.scaleY = (stage.stageWidth - 100) / intro.width;
			intro.x = ((stage.stageWidth - intro.width) / 2);
			intro.y = ((stage.stageHeight - intro.height) / 2);
			intro.touchable = false;
			intro.loop = false;
			this.addChild(intro);
			Starling.juggler.add(intro);
			intro.addEventListener(Event.COMPLETE, function():void {
				setTimeout(nextScreen, 500);
			});

            var measureKeyboard:MeasureKeyboard = new MeasureKeyboard(false);
            measureKeyboard.setKeyboardAdjustNothing();

            initializeFaceBook();

            super.initialize();
        }

        private function initializeFaceBook():void {
            if (!Facebook.isSupported) {
                trace("Facebook ANE is NOT supported on this platform!");
                return;
            }

            // Facebook.logEnabled = true;
            // Facebook.nativeLogEnabled = true;

            var ane:Facebook = Facebook.instance;
            ane.init("736099616784152", _initCallback);
        }

        private function _initCallback(...foo):void {
            trace(foo);
            var ane:Facebook = Facebook.instance;
            ane.setDefaultShareDialogMode(FBShareDialogModeIOS.AUTOMATIC,
                    FBShareDialogModeAndroid.AUTOMATIC);
            ane.setLoginBehavior(FBLoginBehaviorIOS.NATIVE,
                    FBLoginBehaviorAndroid.NATIVE_WITH_FALLBACK);
            ane.setDefaultAudience(FBDefaultAudience.FRIENDS);
        }

        private function nextScreen():void {
            this.dispatchEventWith(EventType.SHOW_DAFTAR_SCREEN);
        }
    }
}