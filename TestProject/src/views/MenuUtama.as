package views {
	import com.freshplanet.ane.AirFacebook.Facebook;
	import com.indramahkota.ane.localnotification.LocalNotificationExtension;
	import com.indramahkota.ane.toast.DurationEnum;
	import com.indramahkota.ane.toast.ToastExtension;

	import constants.EventType;

	import feathers.controls.Alert;
	import feathers.controls.Button;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.Screen;
	import feathers.controls.TextInput;
	import feathers.controls.ToggleSwitch;
	import feathers.controls.WebView;
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.controls.renderers.IListItemRenderer;
	import feathers.core.PopUpManager;
	import feathers.data.ArrayCollection;
	import feathers.events.FeathersEventType;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;

	import flash.desktop.NativeApplication;
	import flash.utils.setTimeout;

	import starling.animation.DelayedCall;
	import starling.core.Starling;
	import starling.display.Canvas;
	import starling.display.DisplayObject;
	import starling.display.MeshBatch;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.TextFormat;
	import starling.utils.Color;

	import utilities.CustomButton;
	import utilities.MaterialDesignSpinner;
	import utilities.ProfileManager;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class MenuUtama extends Screen {
		private var alert:Alert;
		
		private var webView:WebView;
		private var spinner:MaterialDesignSpinner;
		private var exitHandlerBoolean:Boolean = false;
		private var exitoastHandlerBoolean:Boolean = false;
		
		private var list:List;
		private var togToast:ToggleSwitch;
		private var togLocNotif:ToggleSwitch;
		private var tombolSelanjutnya:Button;
		private var inputNamaLengkap:TextInput;
		private var inputNamaHeight:Number;
		private var inputNamaWidth:Number;

		private var traceLabel:Label;
		private var keyboardHeight:Number;
		private var keyboardY:Number;

		private var focusStr:String;
		protected var _data:NavigatorData;

		// private var ane:Facebook;
		
		private var textFormat:TextFormat = new TextFormat("SourceSansPro", 16);
		
		public function MenuUtama() {
			super();
		}

		public function get data():NavigatorData  { return _data; }
		
		public function set data(value:NavigatorData):void  { _data = value; }
		
		override protected function initialize():void {
			this.layout = new AnchorLayout();
			this.backButtonHandler = exitHandler;

			// ane = Facebook.instance;
			
			// MeasureKeyboard.getInstance().setKeyboardAdjustNothing();
			
			var header:Quad = new Quad(stage.stageWidth, 75, 0x008CBA);
			addChild(header);

			var canvasBg:Canvas = new Canvas();
			canvasBg.beginFill(Color.WHITE);
			canvasBg.drawCircle(18, 18, 18);
			canvasBg.endFill();
			
			canvasBg.x = 10 - 2;
			canvasBg.y = 75 - 42 - 2;
			addChildAt(canvasBg, 1);

			var lbl:Label = new Label();
			lbl.text = _data.displayName;
			lbl.styleNameList.add("video-center-label");
			lbl.x = 52;
			lbl.validate();
			lbl.y = (75 - 42) + (32 - lbl.height) / 2;
			addChild(lbl);

			var signOutBtn:CustomButton = new CustomButton();
			signOutBtn.label = "Keluar";
			signOutBtn.styleNameList.add("logout-button");
			signOutBtn.validate();
			signOutBtn.x = stage.stageWidth - signOutBtn.width - 10;
			signOutBtn.y = (75 - 42)  + (32 - signOutBtn.height) / 2;
			signOutBtn.addEventListener(Event.TRIGGERED, attemptSingOut);
			addChild(signOutBtn);

			var img:ImageLoader = new ImageLoader();
			img.source = _data.photoUrl;
			img.setSize(32, 32);
			img.x = 10;
			img.y = 75 - 42;
			this.addChild(img);

			var canvas:Canvas = new Canvas();
			canvas.drawCircle(16, 16, 16);
			img.mask = canvas;

			list = new List();
			list.itemRendererFactory = customItemRendererFactory;
			list.layoutData = new AnchorLayoutData(80, 0, NaN, 0);
			list.isSelectable = false;
			list.clipContent = false;
			addChild(list);

			togToast = new ToggleSwitch();
			togLocNotif = new ToggleSwitch();
			list.dataProvider = new ArrayCollection([
				{"text":"Tampilkan Toast", "accessory":togToast},
				{"text":"Tampilkan Local Notif", "accessory":togLocNotif}
			]);

			list.validate();

			traceLabel = new Label();
			traceLabel.text = "KeyBoard Height + KeyBoard Y";
			traceLabel.width = stage.stageWidth - 20;
			traceLabel.layoutData = new AnchorLayoutData(100 + list.height, NaN, NaN, 10);
			traceLabel.textRendererProperties.wordWrap = true;
			addChild(traceLabel);
						
			inputNamaLengkap = new TextInput();
			inputNamaLengkap.prompt = "Masukkan teks disini!";
			inputNamaLengkap.maxChars = 25;
			inputNamaLengkap.restrict = "a-zA-Z0-9 ";
			inputNamaLengkap.fontStyles = textFormat;
			inputNamaLengkap.promptFontStyles = textFormat;
			inputNamaLengkap.width = stage.stageWidth - 50;
			inputNamaLengkap.nextTabFocus = tombolSelanjutnya;
			inputNamaLengkap.addEventListener(FeathersEventType.FOCUS_IN, onFocusIn);
			inputNamaLengkap.addEventListener(FeathersEventType.FOCUS_OUT, onFocusOut);
			inputNamaLengkap.addEventListener(FeathersEventType.ENTER, onEnter);
			this.addChild(inputNamaLengkap);

			inputNamaLengkap.validate();
			inputNamaHeight = inputNamaLengkap.height;
			inputNamaWidth = inputNamaLengkap.width;
			inputNamaLengkap.y = stage.stageHeight - inputNamaHeight;

			var meshDef:MeshBatch = new MeshBatch();
			var defQuadBg:Quad = new Quad(inputNamaWidth, inputNamaHeight, Color.WHITE);
			var defQuadUp1:Quad = new Quad(inputNamaWidth, 2, Color.GRAY);
			var defQuadUp2:Quad = new Quad(inputNamaWidth, 2, Color.GRAY);
			defQuadUp2.y = inputNamaHeight - 2;
			meshDef.addMesh(defQuadBg);
			meshDef.addMesh(defQuadUp1);
			meshDef.addMesh(defQuadUp2);

			var meshFoc:MeshBatch = new MeshBatch();
			var focQuadBg:Quad = new Quad(inputNamaWidth, inputNamaHeight, Color.WHITE);
			var focQuadUp1:Quad = new Quad(inputNamaWidth, 2, 0x008CBA);
			var focQuadUp2:Quad = new Quad(inputNamaWidth, 2, 0x008CBA);
			focQuadUp2.y = inputNamaHeight - 2;
			meshFoc.addMesh(focQuadBg);
			meshFoc.addMesh(focQuadUp1);
			meshFoc.addMesh(focQuadUp2);

			inputNamaLengkap.backgroundSkin = meshDef;
			inputNamaLengkap.backgroundFocusedSkin = meshFoc;
			
			tombolSelanjutnya = new Button();
			tombolSelanjutnya.width = 50;
			tombolSelanjutnya.height = inputNamaHeight;
			tombolSelanjutnya.label = "OK";
			tombolSelanjutnya.x = inputNamaWidth;
			tombolSelanjutnya.y = inputNamaLengkap.y;
			tombolSelanjutnya.defaultSkin = new Quad(10, 10, 0x008CBA);
			tombolSelanjutnya.downSkin = new Quad(10, 10, 0x006789);
			tombolSelanjutnya.addEventListener(Event.TRIGGERED, selanjutnya);
			this.addChild(tombolSelanjutnya);

			traceLabel.text = "StateAuth: " + _data.stateAuth + "\n" + 
			"Token: " + _data.tokenTest + "\n" +
			"Display Name: " + _data.displayName + "\n" +
			"Photo Url: " + _data.photoUrl;

			super.initialize();
		}

		private function attemptSingOut(event:starling.events.Event):void
		{
			spinner = new MaterialDesignSpinner();
			spinner.color = 0x00BCD4;
			PopUpManager.addPopUp(spinner, true, true, function ():DisplayObject
			{
				var quad:Quad = new Quad(3, 3, 0x000000);
				quad.alpha = 0.50;
				return quad;
			});
			
			if(_data.stateAuth == "webview") {
				//User profile data will be cleared from the app and logged in information will be cleared
				ProfileManager.signOut();

				webView = new WebView();
				webView.addEventListener(Event.COMPLETE, completaHandler);
				webView.loadURL("https://m.facebook.com/logout.php?next=http://m.facebook.com&access_token="+ _data.tokenTest +"&confirm=1");
			} else {
				var delayedCall:DelayedCall = new DelayedCall(function():void
				{
					Starling.juggler.remove(delayedCall);
					showDaftarScreen();
				}, 1);
				Starling.juggler.add(delayedCall);
			}
		}

		private function showDaftarScreen():void {
			PopUpManager.removePopUp(spinner);
			var ane:Facebook = Facebook.instance;
			ane.logOut();
			this.dispatchEventWith(EventType.SHOW_DAFTAR_SCREEN);
		}

		private function completaHandler():void {
			webView.removeEventListener(Event.COMPLETE, completaHandler);    
			webView.dispose();

			if(ToastExtension.isSupported)
			{
				var toast:ToastExtension = new ToastExtension();
				toast.setText("Sign Out");
				toast.setDuration(DurationEnum.LENGTH_LONG);
				toast.show();
			}

			PopUpManager.removePopUp(spinner);
			this.dispatchEventWith(EventType.SHOW_DAFTAR_SCREEN);
		}

		// private function updateTampilan(event:EnterFrameEvent):void {
		// 	traceLabel.text = "[" + event.passedTime + "]\n" + "KeyBoard Height: " 
		// 	+ /* Starling.current.nativeStage.softKeyboardRect.height */ keyboardHeight + 
		// 	" KeyBoard Y: " + /* Starling.current.nativeStage.softKeyboardRect.y */ keyboardY + 
		// 	"\n Stage Width: " + stage.stageWidth + " Stage Height: " + 
		// 	stage.stageHeight + "\nNativeStage Width: " + 
		// 	Starling.current.nativeStage.stageWidth + " NativeStage Height: " + 
		// 	Starling.current.nativeStage.stageHeight + "\nFocus Status: " + focusStr + 
		// 	" inpuY: " + inputNamaLengkap.y;
		// }

		private function onFocusIn():void {
			focusStr = "Focus In";
			setTimeout(function():void {
				inputNamaLengkap.y = (Starling.current.nativeStage.softKeyboardRect.y / Starling.current.contentScaleFactor) - inputNamaHeight - 72;
				tombolSelanjutnya.y = inputNamaLengkap.y;
				keyboardY = (inputNamaLengkap.y + inputNamaHeight) * Starling.current.contentScaleFactor;
				keyboardHeight = (stage.stageHeight - (inputNamaLengkap.y + inputNamaHeight)) * Starling.current.contentScaleFactor;
			}, 150);
		}

		private function onFocusOut():void {
			focusStr = "Focus Out";
			setTimeout(function():void {
				inputNamaLengkap.y = stage.stageHeight - inputNamaHeight;
				tombolSelanjutnya.y = inputNamaLengkap.y;
				keyboardY = 0;
				keyboardHeight = 0; 
			}, 150);
		}

		private function onEnter():void {
			setTimeout(function():void {
				inputNamaLengkap.clearFocus();
				inputNamaLengkap.y = stage.stageHeight - inputNamaHeight;
				tombolSelanjutnya.y = inputNamaLengkap.y;
				keyboardY = 0;
				keyboardHeight = 0;
			}, 150);

			selanjutnya();
		}

		private function customItemRendererFactory():IListItemRenderer
		{
			var itemRenderer:DefaultListItemRenderer = new DefaultListItemRenderer();
			itemRenderer.labelField = "text";
			itemRenderer.padding = 20;

			itemRenderer.accessoryField = "accessory";
			return itemRenderer;
		}
		
		private function selanjutnya():void {
			if(LocalNotificationExtension.isSupported && togLocNotif.isSelected) {
				var notif:LocalNotificationExtension = new LocalNotificationExtension();
				if(inputNamaLengkap.text.length > 0)
					notif.show("Pesan", inputNamaLengkap.text, "Notifikasi baru");
				else
					notif.show();
			}

			if(ToastExtension.isSupported && togToast.isSelected) {
				var toast:ToastExtension = new ToastExtension();
				if(inputNamaLengkap.text.length > 0)
					toast.setText(inputNamaLengkap.text);
				else
					toast.setText("Pesan Toast");
				toast.setDuration(DurationEnum.LENGTH_LONG);
				toast.show();
			}
		}
		
		private function exitHandler():void {
			exitHandlerBoolean = !exitHandlerBoolean;
			if(exitHandlerBoolean || exitoastHandlerBoolean) {
				if(!exitoastHandlerBoolean) {
					if(ToastExtension.isSupported) {
						var toast:ToastExtension = new ToastExtension();
						toast.setText("Tekan sekali lagi untuk keluar");
						toast.setDuration(DurationEnum.LENGTH_LONG);
						toast.show();
					}
					var delayedCall:DelayedCall = new DelayedCall(function():void {
						Starling.juggler.remove(delayedCall);
						exitHandlerBoolean = !exitHandlerBoolean;
					}, 3);
					Starling.juggler.add(delayedCall);
				} else {
					exitHandlerBoolean = !exitHandlerBoolean;
				}
			} else {
				NativeApplication.nativeApplication.exit();
			}
		}
	}
}