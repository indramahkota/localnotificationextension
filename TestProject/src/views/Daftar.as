﻿package views
{
	import com.freshplanet.ane.AirFacebook.Facebook;
	import com.indramahkota.ane.toast.DurationEnum;
	import com.indramahkota.ane.toast.ToastExtension;

	import constants.EventType;
	import constants.URL;

	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.controls.Panel;
	import feathers.controls.Screen;
	import feathers.controls.WebView;
	import feathers.core.PopUpManager;
	import feathers.events.FeathersEventType;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.VerticalLayoutData;

	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;

	import starling.animation.DelayedCall;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextFormat;
	import starling.utils.Color;

	import utilities.MaterialDesignSpinner;
	import utilities.ProfileManager;
	import utilities.RoundedQuad;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Daftar extends Screen
	{
		private var popup:Panel;
		private var webView:WebView;
		private var sessionId:String;
		private var requestUri:String;

		private var isOpen:Boolean;
		protected var _data:NavigatorData;
		
		private var button:Button;
		private var button1:Button;
		private var spinner:MaterialDesignSpinner;
		private var exitHandlerBoolean:Boolean = false;
		private var exitoastHandlerBoolean:Boolean = false;

		// private var ane:Facebook;
		private var traceLabel:Label;
		
		private var textFormat:TextFormat = new TextFormat("SourceSansPro", 24, Color.WHITE);
		
		public function Daftar()
		{
			super();
		}

		public function get data():NavigatorData  { return _data; }
		
		public function set data(value:NavigatorData):void  { _data = value; }
		
		override protected function initialize():void
		{
			this.layout = new AnchorLayout();
			this.backButtonHandler = exitHandler;

			button = new Button();
			button.styleProvider = null;
			button.padding = 10;
			button.label = "Facebook (WebView)";
			button.scaleWhenDown = 0.95;
			button.fontStyles = textFormat;
			button.addEventListener(starling.events.Event.TRIGGERED, function ():void
			{
				startAuth("facebook.com");
			});
			this.addChild(button);

			button.validate();

			button.defaultSkin = new RoundedQuad(10, button.width, 50, 0x3b5998);
			button.x = ((stage.stageWidth - button.width) / 2);
			button.y = ((stage.stageHeight - 50) / 2) - 35;

			button1 = new Button();
			button1.styleProvider = null;
			button1.padding = 10;
			button1.label = "Facebook (Direct)";
			button1.scaleWhenDown = 0.95;
			button1.fontStyles = textFormat;
			button1.addEventListener(starling.events.Event.TRIGGERED, function ():void
			{
				var ane:Facebook = Facebook.instance;
				ane.logInWithReadPermissions(
					["public_profile", "email"], _logInWithReadPermissionsCallback
				);
			});
			this.addChild(button1);
			
			button1.defaultSkin = new RoundedQuad(10, button.width, 50, 0x3b5998);
			button1.x = ((stage.stageWidth - button.width) / 2);
			button1.y = ((stage.stageHeight - 50) / 2) + 35;

			traceLabel = new Label();
			traceLabel.width = stage.stageWidth - 20;
			traceLabel.layoutData = new AnchorLayoutData(NaN, NaN, 10, 10);
			traceLabel.textRendererProperties.wordWrap = true;
			addChild(traceLabel);

			super.initialize();
		}
		
		private function _initCallback(...foo):void {
            trace(foo);
        }

		private function _logInWithReadPermissionsCallback(success:Boolean, userCancelled:Boolean, error:String = null):void {
            trace("logInWithReadPermissions success:" + success +
                " userCancelled:" + userCancelled +
                " error:" + error);

			var str:String;

			if(success) {
				str = "Login success";
				_data.stateAuth = "direct";

				var ane:Facebook = Facebook.instance;
				_data.tokenTest = ane.accessToken.tokenString;
				_data.photoUrl = "https://graph.facebook.com/" + ane.accessToken.userID + "/picture";

				getUserProfile(); //mengatasi error saat mengambil profil
				
			} else if(userCancelled) {
				str = "Login cancelled";
			} else if(error){
				str = error;
			}

			if(ToastExtension.isSupported)
			{
				var toast:ToastExtension = new ToastExtension();
				toast.setText(str);
				toast.setDuration(DurationEnum.LENGTH_LONG);
				toast.show();
			}
        }

		private function getUserProfile():void {
			var ane:Facebook = Facebook.instance;
			
			if(ane.profile) {
				var nama:String = ane.profile.name;
				
				_data.displayName = nama;
				nextScreenHandler();
				
				if(ToastExtension.isSupported)
				{
					var toast:ToastExtension = new ToastExtension();
					toast.setText("LogIn as: " + nama);
					toast.setDuration(DurationEnum.LENGTH_LONG);
					toast.show();
				}

				trace("profile success");
			} else {
				trace("profile gagal");

				spinner = new MaterialDesignSpinner();
				spinner.color = 0x00BCD4;
				PopUpManager.addPopUp(spinner, true, true, function ():DisplayObject
				{
					var quad:Quad = new Quad(3, 3, 0x000000);
					quad.alpha = 0.50;
					return quad;
				});

				var delayedCall:DelayedCall = new DelayedCall(function():void
				{
					kembaliMencobaMengambilProfile(delayedCall);
				}, 0.5);
				Starling.juggler.add(delayedCall);
			}
		}

		private function kembaliMencobaMengambilProfile(dcal:DelayedCall):void {
			trace("mencoba kembali mengambil profile");

			var ane:Facebook = Facebook.instance;
			var toastTxt:String = "";

			if(ane.profile) {
				var nama:String = ane.profile.name;
				toastTxt = "LogIn as: " + nama;

				_data.displayName = nama;
				nextScreenHandler();
				
			} else {
				toastTxt = "LogIn gagal: coba login kembali";
				trace("profile gagal seutuhnya");
			}

			if(ToastExtension.isSupported)
			{
				var toast:ToastExtension = new ToastExtension();
				toast.setText(toastTxt);
				toast.setDuration(DurationEnum.LENGTH_LONG);
				toast.show();
			}
			
			PopUpManager.removePopUp(spinner);
			Starling.juggler.remove(dcal);
			nextScreenHandler();
		}
		
		private function nextScreenHandler():void {
			this.dispatchEventWith(EventType.SHOW_MENU_UTAMA);
		}

		private function startAuth(provider:String):void
		{
			spinner = new MaterialDesignSpinner();
			spinner.color = 0x00BCD4;
			PopUpManager.addPopUp(spinner, true, true, function ():DisplayObject
			{
				var quad:Quad = new Quad(3, 3, 0x000000);
				quad.alpha = 0.50;
				return quad;
			});

			var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");

			var myObject:Object = new Object();
			myObject.continueUri = URL.FIREBASE_REDIRECT_URL;
			myObject.providerId = provider;

			var request:URLRequest = new URLRequest(URL.FIREBASE_CREATE_AUTH_URL);
			request.method = URLRequestMethod.POST;
			request.data = JSON.stringify(myObject);
			request.requestHeaders.push(header);

			var loader:URLLoader = new URLLoader();
			loader.addEventListener(flash.events.Event.COMPLETE, authURLCreated);
			loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			loader.load(request);
		}

		private function authURLCreated(event:flash.events.Event):void
		{
			event.currentTarget.removeEventListener(flash.events.Event.COMPLETE, authURLCreated);

			var rawData:Object = JSON.parse(event.currentTarget.data);

			//We store the sessionId value from the response
			sessionId = rawData.sessionId;

			webView = new WebView();
			webView.width = stage.stageWidth - 50;
			webView.height = stage.stageHeight - 200;
			webView.addEventListener(FeathersEventType.LOCATION_CHANGE, changeLocation);
			webView.layoutData = new VerticalLayoutData(100, 100);

			//We load the URL from the response, it will automatically contain the client id, scopes and the redirect URL
			webView.loadURL(rawData.authUri);

			/* popup = new Panel();
			popup.title = "Sign In";
			popup.width = stage.stageWidth - 50;
			popup.height = stage.stageHeight - 200;
			popup.addChild(webView);

			var closeIcon:ImageLoader = new ImageLoader();
			closeIcon.source = Main.assets.getTextureAtlas("AssetsImages").getTexture("salah24dp");
			closeIcon.width = closeIcon.height = 32;

			var closeButton:Button = new Button();
			closeButton.addEventListener(starling.events.Event.TRIGGERED, function ():void
			{
				isOpen = false;
				webView.dispose();
				PopUpManager.removePopUp(popup, true);
			});
			closeButton.styleNameList.add("header-button");
			closeButton.defaultIcon = closeIcon;
			popup.headerProperties.rightItems = new <DisplayObject>[closeButton]; */

			PopUpManager.removePopUp(spinner);
			PopUpManager.overlayFactory = popUpOverlayFactory;
			PopUpManager.addPopUp(webView, true, true);

			isOpen = true;
		}

		private function popUpOverlayFactory():DisplayObject
		{
			var quad:Quad = new Quad(3, 3, Color.BLACK);
			quad.alpha = 0.5;
			quad.addEventListener(TouchEvent.TOUCH, function(event:TouchEvent):void {
				var touch:Touch = event.getTouch(quad);
				if(touch) {
					if(touch.phase == TouchPhase.ENDED) {
						quad.removeEventListeners(TouchEvent.TOUCH);
						PopUpManager.removePopUp(webView, true);
						isOpen = false;
					}
            	}
			})
			return quad;
		}

		private function changeLocation(event:starling.events.Event):void
		{
			var location:String = webView.location;

			if (location.indexOf("/__/auth/handler?code=") != -1 || location.indexOf("/__/auth/handler?state=") != -1 || location.indexOf("/__/auth/handler#state=") != -1) {

				//We are looking for a code parameter in the URL, once we have it we dispose the webview and prepare the last URLRequest	
				webView.removeEventListener(FeathersEventType.LOCATION_CHANGE, changeLocation);
				// webView.dispose();

				PopUpManager.removePopUp(webView, true);
				isOpen = false;

				requestUri = location;
				getAccountInfo();
			}
		}

		private function getAccountInfo():void
		{
			var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");

			var myObject:Object = new Object();
			myObject.requestUri = requestUri;
			myObject.sessionId = sessionId;
			myObject.returnSecureToken = true;

			var request:URLRequest = new URLRequest(URL.FIREBASE_VERIFY_ASSERTION_URL);
			request.method = URLRequestMethod.POST;
			request.data = JSON.stringify(myObject);
			request.requestHeaders.push(header);

			var loader:URLLoader = new URLLoader();
			loader.addEventListener(flash.events.Event.COMPLETE, registerComplete);
			loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			loader.load(request);
		}


		private function registerComplete(event:flash.events.Event):void
		{
			event.currentTarget.removeEventListener(flash.events.Event.COMPLETE, registerComplete);

			//The profile data is returned back from Firebase, we call our ProfileManager and save the data in a local file

			var rawData:Object = JSON.parse(event.currentTarget.data);
			trace(event.currentTarget.data);
			ProfileManager.saveProfile(rawData);

			trace(rawData.displayName);
			trace(rawData.photoUrl);

			_data.stateAuth = "webview";
			_data.tokenTest = rawData.oauthAccessToken;
			_data.displayName = rawData.displayName;
			_data.photoUrl = rawData.photoUrl;

			if(ToastExtension.isSupported)
			{
				var toast:ToastExtension = new ToastExtension();
				toast.setText("LogIn as: " + rawData.displayName);
				toast.setDuration(DurationEnum.LENGTH_LONG);
				toast.show();
			}

			this.dispatchEventWith(EventType.SHOW_MENU_UTAMA);
		}

		private function errorHandler(event:IOErrorEvent):void
		{
			button.touchable = true;
			trace(event.currentTarget.data);
		}
		
		private function exitHandler():void
		{
			exitHandlerBoolean = !exitHandlerBoolean;
			
			if(exitHandlerBoolean || exitoastHandlerBoolean)
			{
				if(!exitoastHandlerBoolean)
				{
					if(ToastExtension.isSupported)
					{
						var toast:ToastExtension = new ToastExtension();
						toast.setText("Tekan sekali lagi untuk keluar");
						toast.setDuration(DurationEnum.LENGTH_LONG);
						toast.show();
					}
					
					var delayedCall:DelayedCall = new DelayedCall(function():void
					{
						Starling.juggler.remove(delayedCall);
						exitHandlerBoolean = !exitHandlerBoolean;
					}, 3);
					Starling.juggler.add(delayedCall);
				}
				else
				{
					exitHandlerBoolean = !exitHandlerBoolean;
				}
			}
			else
			{
				NativeApplication.nativeApplication.exit();
			}
		}
	}
}