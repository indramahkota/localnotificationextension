﻿package {
	import constants.EventType;
	import constants.ScreenID;

	import feathers.controls.StackScreenNavigator;
	import feathers.controls.StackScreenNavigatorItem;

	import starling.extensions.animate.AnimAssetManager;

	import themes.CustomTheme;

	import views.Daftar;
	import views.Intro;
	import views.MenuUtama;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Main extends StackScreenNavigator {
		private var navigatorData:NavigatorData;
		public static var assets:AnimAssetManager;
		
		public function Main() {}
		
		public function start(_assets:AnimAssetManager):void {
			assets = _assets;

			new CustomTheme();
			navigatorData = new NavigatorData();

			var intro:StackScreenNavigatorItem = new StackScreenNavigatorItem(Intro);
			intro.properties.data = navigatorData;
			intro.setScreenIDForPushEvent(EventType.SHOW_DAFTAR_SCREEN, ScreenID.DAFTARSCREEN);
			intro.setScreenIDForPushEvent(EventType.SHOW_MENU_UTAMA, ScreenID.MENUUTAMA);
			this.addScreen(ScreenID.INTROSCREEN, intro);
			
			var daftar:StackScreenNavigatorItem = new StackScreenNavigatorItem(Daftar);
			daftar.properties.data = navigatorData;
			daftar.setScreenIDForPushEvent(EventType.SHOW_MENU_UTAMA, ScreenID.MENUUTAMA);
			this.addScreen(ScreenID.DAFTARSCREEN, daftar);

			var menuutama:StackScreenNavigatorItem = new StackScreenNavigatorItem(MenuUtama);
			menuutama.properties.data = navigatorData;
			menuutama.setScreenIDForPushEvent(EventType.SHOW_DAFTAR_SCREEN, ScreenID.DAFTARSCREEN);
			this.addScreen(ScreenID.MENUUTAMA, menuutama);

			this.rootScreenID = ScreenID.INTROSCREEN;
		}
	}
}